#ifndef TESTERHEADER_H_INCLUDED
#define TESTERHEADER_H_INCLUDED

#include "AbstractData.h"

typedef struct fisiere
{
    FILE *f;
    FILE *g;
}fisiere;

typedef struct VectorStructures
{
    char denumire;
    void *structura;
}VectorStructures;

void * determinaStructura(VectorStructures vs[10], int sizevs, char c);

void determineFiles(int argc, char** argv, int indice, fisiere *f);
void VectorInt(int argc, char** argv, int indice, int TYPE);
void VectorFloat(int argc, char** argv, int indice, int TYPE);
void VectorChar(int argc, char** argv, int indice, int TYPE);

void ListInt(int argc, char**argv, int indice);

void HashTableInt(int argc, char** argv, int indice, int TYPE);

void MinHeapInt(int argc, char** argv, int indice, int TYPE);

void BST_Int(int argc, char** argv, int indice);
void BST_Int2(int argc, char** argv, int indice);
void BST_Char(int argc, char** argv, int indice);
void BST_String(int argc, char** argv, int indice);

void BalancedBST_Int(int argc, char** argv, int indice);
void BalancedBST_Int2(int argc, char** argv, int indice);
int determinaPozBST(VectorStructures vs[10], int sizevs, char c);
void updateVS(nodeBST *b, VectorStructures vs[10], int sizevs, char c);
#endif // TESTERHEADER_H_INCLUDED
