#include "testerHeader.h"
#define TYPE 4
void ListInt(int argc, char** argv, int indice)//indice = al catelea fisier din argv
{
    fisiere f;
    determineFiles(argc,argv,indice,&f);

    char comanda[30];
    LinkedList *l, *ll;

    char c, k;
    VectorStructures vs[10];
    int sizevs = 0;

    while(!feof(f.f))
    {
        fscanf(f.f,"%s %c ",comanda,&c);

        if(!strcmp(comanda, "CreateLinkedList"))
        {
            l = CreateLinkedList(printInt, cmpNumbers);
            vs[sizevs].structura = l;
            vs[sizevs].denumire = c;
            sizevs++;
        }
        else if(!strcmp(comanda, "PrintLinkedList"))
        {
            l = (LinkedList*)determinaStructura(vs,sizevs, c);
            PrintLinkedList(l,f.g);
        }
        else if(!strcmp(comanda, "AddLinkedListItem"))
        {
            l = (LinkedList*)determinaStructura(vs,sizevs, c);
            void *item=(int*)malloc(TYPE);
            fscanf(f.f,"%d ",(int*)item);

            AddLinkedListItem(l,item);
        }
        else if(!strcmp(comanda, "PutLinkedListItem"))
        {
            l = (LinkedList*)determinaStructura(vs,sizevs, c);
            int index;
            void *item=(int*)malloc(TYPE);

            fscanf(f.f,"%d ",&index);
            fscanf(f.f,"%d ",(int*)item);

            PutLinkedListItem(l,item,index);
        }
        else if(!strcmp(comanda, "GetLinkedListItem"))
        {
            l = (LinkedList*)determinaStructura(vs,sizevs, c);
            node *n = (node*)malloc(sizeof(node));
            int index;

            fscanf(f.f,"%d ",&index);
            n=GetLinkedListItem(l,index);
            if( n == NULL)
                fprintf(f.g, "Elementul luat este NULL!\n");
            else
                fprintf(f.g,"%d \n",*((int*)n->data));
        }
        else if(!strcmp(comanda, "DeleteLinkedListItem"))
        {
            l = (LinkedList*)determinaStructura(vs,sizevs, c);
            void *item=(int*)malloc(TYPE);
            node *n = CreateNode(0);

            fscanf(f.f,"%d ",(int*)item);
            n=DeleteLinkedListItem(l,item);
            if(n == NULL)
                fprintf(f.g,"Nu s-a efectuat stergerea\n");
            else
                fprintf(f.g,"%d\n",*((int*)n->data));
        }
        else if(!strcmp(comanda, "SearchLinkedListItem"))
        {
            l = (LinkedList*)determinaStructura(vs,sizevs, c);
            void *item=(int*)malloc(TYPE);
            node *n = CreateNode(0);

            fscanf(f.f,"%d ",(int*)item);
            n=SearchLinkedListItem(l,item);
            if(n == NULL)
                fprintf(f.g,"Nu s-a gasit elementul\n");
            else
                fprintf(f.g,"%d\n",*((int*)n->data));

        }
        else if(!strcmp(comanda, "SortLinkedList"))
        {
            l = (LinkedList*)determinaStructura(vs,sizevs, c);
            SortLinkedList(l);
        }
        else if(!strcmp(comanda, "DeleteLinkedList"))
        {
            l = (LinkedList*)determinaStructura(vs,sizevs, c);
            DeleteLinkedList(l);
        }
        else if(!strcmp(comanda, "MergeLinkedLists"))
        {
            fscanf(f.f,"%c ",&k);
            l = (LinkedList*)determinaStructura(vs,sizevs, c);
            ll = (LinkedList*)determinaStructura(vs,sizevs, k);
            MergeLinkedLists(l, ll);
        }
    }
    fclose(f.f);
    fclose(f.g);
}
