#include "testerHeader.h"

void HashTableInt(int argc, char** argv, int indice, int TYPE)//indice = al catelea fisier din argv
{
    fisiere f;
    determineFiles(argc,argv,indice,&f);

    char comanda[30];
    char c;
    HashTable *hT;

    VectorStructures vs[10];
    int sizevs = 0;

    while(!feof(f.f))
    {
        fscanf(f.f,"%s %c ",comanda,&c);

        if(!strcmp(comanda, "CreateHashTable"))
        {
            hT = CreateHashTable(15, hashFunctionInt, printInt, cmpNumbers);
            vs[sizevs].structura = hT;
            vs[sizevs].denumire = c;
            sizevs++;
        }
        else if(!strcmp(comanda, "PrintHashTable"))
        {
            hT = (HashTable*)determinaStructura(vs,sizevs, c);
            if(hT != NULL)
            {
                PrintHashTable(hT, f.g);
            }
        }
        else if(!strcmp(comanda, "AddHashTableItem"))
        {
            int nritems;

            fscanf(f.f,"%d ",&nritems);

            hT = (HashTable*)determinaStructura(vs,sizevs, c);
                if(hT != NULL)
                {
                    for(int i=0; i < nritems; i++)
                    {
                        void* value = (int*)malloc(TYPE);
                        fscanf(f.f,"%d ",(int*)value);
                        AddHashTableItem(hT,value,value);
                    }
                }
        }
        else if(!strcmp(comanda, "GetHashTableItem"))
        {
            void* key = (int*)malloc(TYPE);
            //void *item
            Element *e = (Element*)malloc(sizeof(Element*));

            fscanf(f.f,"%d ",(int*)key);

            hT = (HashTable*)determinaStructura(vs,sizevs, c);
            if(hT != NULL)
            {
                e = (Element*)GetHashTableElement(hT,key);
                if(e != NULL)
                    fprintf(f.g,"%d \n",*((int*)e->value));
                else
                    fprintf(f.g, "Elementul luat este NULL!\n");
            }
        }
        else if(!strcmp(comanda, "DeleteHashTableItem"))
        {
            void* key = (int*)malloc(TYPE);

            fscanf(f.f,"%d ",(int*)key);

            hT = (HashTable*)determinaStructura(vs,sizevs, c);
            if(hT != NULL)
                DeleteHashTableItem(hT,key,key);
        }
        else if(!strcmp(comanda, "SearchHashTableItem"))
        {
            void* value = (int*)malloc(TYPE);

            fscanf(f.f,"%d ",(int*)value);

            hT = (HashTable*)determinaStructura(vs,sizevs, c);
            if(hT != NULL)
            {
                if(SearchHashTableItem(hT,value,value))
                    fprintf(f.g,"Element gasit!\n");
                else
                    fprintf(f.g, "Elementul nu a fost gasit!\n");
            }
        }
        else if(!strcmp(comanda, "ReHashTable"))
        {
            HashTable *hhT = CreateHashTable(1, 0, printInt, cmpNumbers);
            hT = (HashTable*)determinaStructura(vs,sizevs, c);
            if(hT != NULL)
                hhT = ReHashTable(hT,hashFunctionInt2);
            PrintHashTable(hhT, f.g);
        }
        else if(!strcmp(comanda, "DeleteHashTable"))
        {
            hT = (HashTable*)determinaStructura(vs, sizevs, c);
            if(hT != NULL)
            {
                DeleteHashTable(hT);
                for(int i=0; i < sizevs;i++)
                {
                    if(vs[i].denumire == c)
                    {
                        vs[i].structura = NULL;
                        break;
                    }
                }
            }
        }
    }
    fclose(f.f);
    fclose(f.g);
}
