#include "AbstractData.h"

BalancedBST* CreateBalancedBST(void (*print) (const void*, FILE*), int (*cmp) (const void*, const void*))
{
    BalancedBST* b = malloc(sizeof(BalancedBST));
    b->root = NULL;
    b->cmp = cmp;
    b->print = print;
    return b;
}

nodeAVL* CreateNodeAVL(void *key, void* data)
{
    nodeAVL* n = malloc(sizeof(nodeAVL));
    n->key = key;
    n->data = data;
    n->left = NULL;
    n->right = NULL;

    return n;
}

void PrintBalancedBST(BalancedBST* b, FILE *f)
{
    PrintBalancedBSTRecursive(b->root, f, b->print, 0);
}
void PrintBalancedBSTRecursive(nodeAVL* root, FILE* f, void (*print) (const void*, FILE*), int level)
{
    if(root != NULL)
    {
        PrintBalancedBSTRecursive(root->left, f, print, level + 1);

        for(int i = 0; i <= level; i++)
            fprintf(f, "  ");
        print(root->data, f);
        fprintf(f, "\n");

        PrintBalancedBSTRecursive(root->right, f, print, level + 1);
    }
}

void PreorderBalancedBST(BalancedBST* b, FILE *f)
{
    PreorderBalancedBSTRecursive(b->root, f, b->print);
    fprintf(f, "\n");
}

void InorderBalancedBST(BalancedBST* b, FILE *f)
{
    InorderBalancedBSTRecursive(b->root, f, b->print);
    fprintf(f, "\n");
}

void PostorderBalancedBST(BalancedBST* b, FILE *f)
{
    PostorderBalancedBSTRecursive(b->root, f, b->print);
    fprintf(f, "\n");
}

void PreorderBalancedBSTRecursive(nodeAVL* root, FILE* f, void (*print) (const void*, FILE*))
{
    if(root == NULL)
    {
        return ;
    }
    print(root->data, f);
    PreorderBalancedBSTRecursive(root->left, f, print);
    PreorderBalancedBSTRecursive(root->right, f, print);
}

void InorderBalancedBSTRecursive(nodeAVL* root, FILE* f, void (*print) (const void*, FILE*))
{
    if(root == NULL)
    {
        return ;
    }
    InorderBalancedBSTRecursive(root->left, f, print);
    print(root->data, f);
    InorderBalancedBSTRecursive(root->right, f, print);
}

void PostorderBalancedBSTRecursive(nodeAVL* root, FILE* f, void (*print) (const void*, FILE*))
{
    if(root == NULL)
    {
        return ;
    }
    PostorderBalancedBSTRecursive(root->left, f, print);
    PostorderBalancedBSTRecursive(root->right, f, print);
    print(root->data, f);
}

void AddBalancedBSTItem(BalancedBST* b, void *key, void *data)
{
    b->root = AddBalancedBSTItemRecursive(b->root, key, data, b->cmp);
}

nodeAVL* AddBalancedBSTItemRecursive(nodeAVL* root, void *key, void *data, int (*cmp) (const void*, const void*))
{
    if(root == NULL)
        return(CreateNodeAVL(key, data));
    if(cmp(key, root->key) <= 0)
        root->left = AddBalancedBSTItemRecursive(root->left, key, data, cmp);
    else
        root->right = AddBalancedBSTItemRecursive(root->right, key, data, cmp);
    return BalanceAVL(root);
}

nodeAVL* SearchBalancedBSTItem(BalancedBST* b, void *key)
{
    nodeAVL *n = SearchBalancedBSTItemRecursive(b->root, key, b->cmp);
    return n;
}
nodeAVL* SearchBalancedBSTItemRecursive(nodeAVL* root, void *key, int (*cmp) (const void*, const void*))
{
    int i;
    while(root != NULL)
    {
        i = cmp(root->key, key);
        if(i == 0)
        {
            return root;
        }
        if(i > 0)
        {
            root = root->left;
        }
        else
        {
            root = root->right;
        }
    }
    return NULL;
}

nodeAVL* FindMinAVL(nodeAVL* root)
{
    while(root->left != NULL)
        root = root->left;
    return root;
}

nodeAVL* DeleteBalancedBSTItem(BalancedBST *b, void *key)
{
    nodeAVL *n = DeleteBalancedBSTItemRecursive(b->root, key, b->cmp);
    return n;
}
nodeAVL* DeleteBalancedBSTItemRecursive(nodeAVL* root, void *key, int (*cmp) (const void*, const void*))
{
    nodeAVL *n;
    n = SearchBalancedBSTItemRecursive(root, key, cmp);

    if(n != NULL)
    {
        n = CreateNodeAVL(n->key, n->data);
        root = DeleteBalancedBSTItemReturnRoot(root, key, cmp);
    }
    return n;
}

nodeAVL* DeleteBalancedBSTItemReturnRoot(nodeAVL* root, void *key, int (*cmp) (const void*, const void*))
{
    nodeAVL *n;

    if(root == NULL)
    {
        return root;
    }

    int comparare = cmp(key, root->key);

    if(comparare < 0)
    {
        root->left = DeleteBalancedBSTItemReturnRoot(root->left, key, cmp);
    }
    else if(comparare > 0)
    {
        root->right = DeleteBalancedBSTItemReturnRoot(root->right, key, cmp);
    }
    else
    {
        if(root->left == NULL)//are numai un fiu si anume pe partea dreapta
        {
            n = root->right;
            free(root);
            return n;
        }
        else if(root->right == NULL) //acum pe partea stg
        {
            n = root->left;
            free(root);
            return n;
        }

        n = FindMinAVL(root->right);//2 fii
        root = CreateNodeAVL(n->key, n->data);
        root->right = DeleteBalancedBSTItemReturnRoot(root->right, n->key, cmp);
    }
    return BalanceAVL(root);
}

int HeightBalancedBST(BalancedBST *b)
{
    return HeightBalancedBSTRecursive(b->root);
}
int HeightBalancedBSTRecursive(nodeAVL* root)
{
    int lh;     //height of left subtree
    int rh;     // height of right subtree

    if(root == NULL)
    {
        return 0;
    }
    else
    {
        lh = HeightBalancedBSTRecursive(root->left);
        rh = HeightBalancedBSTRecursive(root->right);
        return 1 + (lh > rh ? lh : rh);
    }
}

void DeleteBalancedBST(BalancedBST *b)
{
    DeleteBalancedBSTRecursive(b->root);
}
void DeleteBalancedBSTRecursive(nodeAVL* root)
{
    if(root!=0)
        {
            DeleteBalancedBSTRecursive(root->left);
            DeleteBalancedBSTRecursive(root->right);
            free(root);
        }
}

int BalanceFactorAVL(nodeAVL *root)
{
    int bf = 0;

    if(root->left != NULL)
        bf += HeightBalancedBSTRecursive(root->left);
    if(root->right != NULL)
        bf -= HeightBalancedBSTRecursive(root->right);

    return bf;
}

nodeAVL* RotateLeftLeftAVL(nodeAVL* root)
{
    nodeAVL *a;
    nodeAVL *b;

    a = root;
    b = a->left;
    a->left = b->right;
    b->right = a;

    return b;
}

nodeAVL* RotateRightRightAVL(nodeAVL* root)
{
    nodeAVL *a;
    nodeAVL *b;

    a = root;
    b = a->right;
    a->right = b->left;
    b->left = a;

    return b;
}

nodeAVL* RotateLeftRightAVL(nodeAVL* root)
{
    nodeAVL *a;
    nodeAVL *b;
    nodeAVL *c;

    a = root;
    b = a->left;
    c = b->right;
    a->left = c->right;
    b->right = c->left;
    c->left = b;
    c->right = a;

    return c;
}

nodeAVL* RotateRightLeftAVL(nodeAVL* root)
{
    nodeAVL *a;
    nodeAVL *b;
    nodeAVL *c;

    a = root;
    b = a->right;
    c = b->left;
    a->right = c->left;
    b->left = c->right;
    c->right = b;
    c->left = a;

    return c;
}

nodeAVL* BalanceAVL(nodeAVL* root)
{
    nodeAVL* newRoot = NULL;

    if(root->left != NULL)
        root->left = BalanceAVL(root->left);
    if(root->right != NULL)
        root->right = BalanceAVL(root->right);

    int bf = BalanceFactorAVL(root);

    if( bf >= 2 )
    {
		if(BalanceFactorAVL(root->left) <= -1)
			newRoot = RotateLeftRightAVL(root);
		else
			newRoot = RotateLeftLeftAVL(root);
	}
	else if( bf <= -2 )
    {
		if( BalanceFactorAVL(root->right) >= 1 )
			newRoot = RotateRightLeftAVL(root);
		else
			newRoot = RotateRightRightAVL(root);
	}
	else
    {
		newRoot = root;
	}

	return newRoot;
}

void MergeBalancedBSTs(BalancedBST *b1, BalancedBST *b2)
{
    MergeBalancedBSTsRecursive(b1->root, b2->root, b1->cmp);
}
void MergeBalancedBSTsRecursive(nodeAVL *root1, nodeAVL *root2, int (*cmp) (const void*, const void*))
{
    if(root2 == NULL)
    {
        return ;
    }
    MergeBalancedBSTsRecursive(root1, root2->left, cmp);
    AddBalancedBSTItemRecursive(root1, root2->key, root2->data, cmp);
    MergeBalancedBSTsRecursive(root1, root2->right, cmp);
}
