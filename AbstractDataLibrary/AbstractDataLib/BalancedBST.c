#include "AbstractData.h"
/*
nodeAVL* CreateBalancedBST()
{
    nodeAVL* root = NULL;
    return root;
}

nodeAVL* CreateNodeAVL(void *key, void* data)
{
    nodeAVL* n = malloc(sizeof(nodeAVL));
    n->key = key;
    n->data = data;
    n->left = NULL;
    n->right = NULL;

    return n;
}

void PrintBalancedBST(nodeAVL* root, FILE* f, void (*print) (const void*, FILE*),int level)
{
    if(root != NULL)
    {
        PrintBalancedBST(root->left, f, print, level + 1);

        for(int i = 0; i <= level; i++)
            fprintf(f, "  ");
        print(root->data, f);
        fprintf(f, "\n");

        PrintBalancedBST(root->right, f, print, level + 1);
    }
}

void PreorderBalancedBST(nodeAVL* root, FILE* f, void (*print) (const void*, FILE*))
{
    if(root == NULL)
    {
        return ;
    }
    print(root->data, f);
    PreorderBalancedBST(root->left, f, print);
    PreorderBalancedBST(root->right, f, print);
}

void InorderBalancedBST(nodeAVL* root, FILE* f, void (*print) (const void*, FILE*))
{
    if(root == NULL)
    {
        return ;
    }
    InorderBalancedBST(root->left, f, print);
    print(root->data, f);
    InorderBalancedBST(root->right, f, print);
}

void PostorderBalancedBST(nodeAVL* root, FILE* f, void (*print) (const void*, FILE*))
{
    if(root == NULL)
    {
        return ;
    }
    PostorderBalancedBST(root->left, f, print);
    PostorderBalancedBST(root->right, f, print);
    print(root->data, f);
}

nodeAVL* AddBalancedBSTItem(nodeAVL* root, void *key, void *data, int (*cmp) (const void*, const void*))
{
    if(root == NULL)
        return(CreateNodeAVL(key, data));
    if(cmp(key, root->key) <= 0)
        root->left = AddBalancedBSTItem(root->left, key, data, cmp);
    else
        root->right = AddBalancedBSTItem(root->right, key, data, cmp);
    return BalanceAVL(root);
}

nodeAVL* SearchBalancedBSTItem(nodeAVL* root, void *key, int (*cmp) (const void*, const void*))
{
    int i;
    while(root != NULL)
    {
        i = cmp(root->key, key);
        if(i == 0)
        {
            return root;
        }
        if(i > 0)
        {
            root = root->left;
        }
        else
        {
            root = root->right;
        }
    }
    return NULL;
}

nodeAVL* FindMinAVL(nodeAVL* root)
{
    while(root->left != NULL)
        root = root->left;
    return root;
}


nodeAVL* DeleteBalancedBSTItem(nodeAVL* root, void *key, int (*cmp) (const void*, const void*))
{
    nodeAVL *n;
    n = SearchBalancedBSTItem(root, key, cmp);

    if(n != NULL)
    {
        n = CreateNodeAVL(n->key, n->data);
        root = DeleteBalancedBSTItemReturnRoot(root, key, cmp);
    }
    return n;
}

nodeAVL* DeleteBalancedBSTItemReturnRoot(nodeAVL* root, void *key, int (*cmp) (const void*, const void*))
{
    nodeAVL *n;

    if(root == NULL)
    {
        return root;
    }

    int comparare = cmp(key, root->key);

    if(comparare < 0)
    {
        root->left = DeleteBalancedBSTItemReturnRoot(root->left, key, cmp);
    }
    else if(comparare > 0)
    {
        root->right = DeleteBalancedBSTItemReturnRoot(root->right, key, cmp);
    }
    else
    {
        if(root->left == NULL)//are numai un fiu si anume pe partea dreapta
        {
            n = root->right;
            free(root);
            return n;
        }
        else if(root->right == NULL) //acum pe partea stg
        {
            n = root->left;
            free(root);
            return n;
        }

        n = FindMinAVL(root->right);//2 fii
        root = CreateNodeAVL(n->key, n->data);
        root->right = DeleteBalancedBSTItemReturnRoot(root->right, n->key, cmp);
    }
    return BalanceAVL(root);
}

int HeightBalancedBST(nodeAVL* root)
{
    int lh;     //height of left subtree
    int rh;     // height of right subtree

    if(root == NULL)
    {
        return 0;
    }
    else
    {
        lh = HeightBalancedBST(root->left);
        rh = HeightBalancedBST(root->right);
        return 1 + (lh > rh ? lh : rh);
    }
}

void DeleteBalancedBST(nodeAVL* root)
{
    if(root!=0)
        {
            DeleteBalancedBST(root->left);
            DeleteBalancedBST(root->right);
            free(root);
        }
}

int BalanceFactorAVL(nodeAVL *root)
{
    int bf = 0;

    if(root->left != NULL)
        bf += HeightBalancedBST(root->left);
    if(root->right != NULL)
        bf -= HeightBalancedBST(root->right);

    return bf;
}

nodeAVL* RotateLeftLeftAVL(nodeAVL* root)
{
    nodeAVL *a;
    nodeAVL *b;

    a = root;
    b = a->left;
    a->left = b->right;
    b->right = a;

    return b;
}

nodeAVL* RotateRightRightAVL(nodeAVL* root)
{
    nodeAVL *a;
    nodeAVL *b;

    a = root;
    b = a->right;
    a->right = b->left;
    b->left = a;

    return b;
}

nodeAVL* RotateLeftRightAVL(nodeAVL* root)
{
    nodeAVL *a;
    nodeAVL *b;
    nodeAVL *c;

    a = root;
    b = a->left;
    c = b->right;
    a->left = c->right;
    b->right = c->left;
    c->left = b;
    c->right = a;

    return c;
}

nodeAVL* RotateRightLeftAVL(nodeAVL* root)
{
    nodeAVL *a;
    nodeAVL *b;
    nodeAVL *c;

    a = root;
    b = a->right;
    c = b->left;
    a->right = c->left;
    b->left = c->right;
    c->right = b;
    c->left = a;

    return c;
}

nodeAVL* BalanceAVL(nodeAVL* root)
{
    nodeAVL* newRoot = NULL;

    if(root->left != NULL)
        root->left = BalanceAVL(root->left);
    if(root->right != NULL)
        root->right = BalanceAVL(root->right);

    int bf = BalanceFactorAVL(root);

    if( bf >= 2 )
    {
		if(BalanceFactorAVL(root->left) <= -1)
			newRoot = RotateLeftRightAVL(root);
		else
			newRoot = RotateLeftLeftAVL(root);
	}
	else if( bf <= -2 )
    {
		if( BalanceFactorAVL(root->right) >= 1 )
			newRoot = RotateRightLeftAVL(root);
		else
			newRoot = RotateRightRightAVL(root);
	}
	else
    {
		newRoot = root;
	}

	return newRoot;
}
*/
