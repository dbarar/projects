#include "AbstractData.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

Vector* CreateVector(int capacity, void (*print) (const void*, FILE*), int (*cmp) (const void*, const void*))
{
    Vector *v = malloc(sizeof(Vector));
    v->sizev = 0;
    v->capacity = capacity;
    v->expensionFactor = 2;
    void **a =(void*) malloc(v->capacity * sizeof(void*));
    if(!a)
    {
        free(v);
        fprintf(stderr,"Nu s-a alocat memorie pentru vector!\n");
    }
    v->a = a;
    v->cmp = cmp;
    v->print = print;
    return v;
}

void PrintVector(Vector *v, FILE *f)
{
    if(v == NULL || v->sizev == 0)
    {
        fprintf(stderr, "Vector inexistent sau vid, (printare)!\n");
    }
    else
    {
        for(int i=0;i < v->sizev; i++)
        {
            v->print(v->a[i],f);
        }
        fprintf(f,"\n");
    }
}
int AddVectorItem(Vector *v, void *item)
{
    if(v == NULL)
    {
        fprintf(stderr, "Vector inexistent!\n");
        return 0;
    }

    if(v->sizev >= v->capacity)
    {
        if(!ExpandCapacity(v))
            return 0;
    }
    v->a[v->sizev] = item;
    (v->sizev)++;
    return 1;
}

int AddVectorItems(Vector *v, void *items, int nrElemente, int type)
{
    if(v == NULL)
    {
        fprintf(stderr, "Vector inexistent!\n");
        return 0;
    }

    if(v->sizev + nrElemente >= v->capacity)
    {
        if(!ExpandCapacity(v))
            return 0;
    }

    int size = v->sizev;
    for(int i=0; i < nrElemente; i++)
        v->a[i + size]=items + i*type;

    v->sizev += nrElemente;
    return 1;
}

int PutVectorItem(Vector *v, void *item, int index)
{
    if(v == NULL)
    {
        fprintf(stderr, "Vector inexistent!\n");
        return 0;
    }

    if(index == v->sizev)
        return AddVectorItem(v, item);

    if(index != v->sizev && !indexVerification(index, v->sizev))
    {
        return 0;
    }

    if(v->sizev >= v->capacity)
    {
        v->capacity *= 2;
        v->a = realloc(v->a, v->capacity);
    }

    int deplasare = (v->sizev - index) * sizeof(void*);
    memmove(&(v->a[index + 1]), &(v->a[index]), deplasare);
    v->a[index] = item;
    (v->sizev)++;
    return 1;
}

void* GetVectorItem(Vector *v, int index)
{
    if(v == NULL || v->sizev == 0)
    {
        fprintf(stderr, "Vector inexistent sau vid, nu se poate lua niciun element!\n");
        return NULL;
    }

    if(!indexVerification(index, v->sizev))
        return NULL;

    return v->a[index];
}
int DeleteVectorItem(Vector *v, int index)
{
    if(v == NULL || v->sizev == 0)
    {
        fprintf(stderr, "Vector inexistent sau vid, nu se poate sterge niciun element!\n");
        return 0;
    }

    if(!indexVerification(index, v->sizev))
    {
        return 0;
    }

    for(int i = index; i < (v->sizev - 1); i++)
        v->a[i] = v->a[i+1];
    (v->sizev)--;
    return 1;
}
int SearchVectorItem(Vector *v, void *item)
{
    if(v == NULL)
    {
        fprintf(stderr, "Vector inexistent!\n");
        return 0;
    }

    if(item == NULL)
        return -1;
    for(int i=0; i < v->sizev; i++)
        if(v->cmp(item, v->a[i]) == 0)
            return i;
    return -1;
}

void SortVector(Vector *v)
{
    if(v == NULL || v->sizev == 0)
    {
        fprintf(stderr, "Vector inexistent sau vid, nu se poate sorta!\n");
    }
    else
    {
        int ok=0;
        do
        {
            ok=0;
            for(int i=0; i < (v->sizev-1); i++)
                if(v->cmp(v->a[i], v->a[i+1]) > 0)
                {
                    ok=1;
                    void* aux = v->a[i];
                    v->a[i] = v->a[i+1];
                    v->a[i+1] = aux;
                }
        }
        while(ok);
    }
}
void DeleteVector(Vector *v)
{
    if(v == NULL)
    {
        fprintf(stderr, "Vector inexistent, nu se poate sterge!\n");
    }
    else
    {
        v->sizev = 0;
        free(v->a);
        free(v);
    }
}

int ExpandCapacity(Vector *v)
{
    v->capacity *= v->expensionFactor;
    void **a = malloc(v->capacity * sizeof(void*));
    if(!a)
    {
        free(a);
        fprintf(stderr,"Nu s-a alocat memorie pentru vector!\n");
        return 0;
    }
    memcpy(a, v->a, v->sizev * sizeof(void*));
    free(v->a);
    v->a = a;
    return 1;
}

int MergeVectors(Vector *v, Vector *w)
{
    if(v == NULL)
    {
        fprintf(stderr, "Vector inexistent!\n");
        return 0;
    }

    if(w == NULL)
    {
        fprintf(stderr, "Vector inexistent!\n");
        return 0;
    }

    if(v->sizev + w->sizev >= v->capacity)
    {
        if(!ExpandCapacity(v))
            return 0;
    }

    int size = v->sizev;
    for(int i=0; i < w->sizev; i++)
        v->a[i + size] = w->a[i];

    v->sizev += w->sizev;
    return 1;
}
