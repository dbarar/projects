#include "AbstractData.h"
#define TABLE_GROW_DENSITY (0.7)
HashTable* CreateHashTable(int capacity, int (*hashFunction)(const void*), void (*print) (const void*, FILE*), int (*cmp) (const void*, const void*))
{
    HashTable *hT = malloc(sizeof(HashTable));
    if(hT == NULL)
        return NULL;
    hT->tableSize = capacity;
    hT->nrElements = 0;
    hT->print = print;
    hT->cmp = cmp;
    if(hashFunction != NULL)
        hT->hashFunction = hashFunction;
    else
        hT->hashFunction = hashFunctionInt;

    hT->table = malloc(sizeof(Element*) * capacity);
    if(hT->table == 0)
    {
        free(hT);
        return 0;
    }

    for(int i = 0; i < hT->tableSize; i++)
        hT->table[i] = NULL;
    return hT;
}
Element* CreateElement(void *key, void *value)
{
    Element* e = malloc(sizeof(Element));
    if(e == NULL)
        return NULL;
    e->key = malloc(sizeof(void*));
    if(e->key == NULL)
        return NULL;
    e->value = malloc(sizeof(void*));
    if(e->value == NULL)
        return NULL;
    e->key = key;
    e->value = value;
    e->next = NULL;
    return e;
}
void PrintHashTable(HashTable *hT, FILE *f)
{
    Element *e;

    for(int i = 0; i < hT->tableSize; i++)
    {
        if(hT->table[i] != NULL)
            for(e = hT->table[i]; e != NULL; e = e->next)
                hT->print(e->value, f);
    }
    fprintf(f,"\n");
}
void AddHashTableItem(HashTable *hT, void *key, void *value)
{
    Element *e;
    Element *next;
    Element *last;

    unsigned long h = hT->hashFunction(key);
    h = h % hT->tableSize;

    //e = GetHashTableElement(hT, key);
    next = hT->table[h];

    while(next != NULL && next->key != NULL && (hT->cmp(key, next->key) > 0))
    {
        last = next;
        next = next->next;
    }
    if(next != NULL && next->key != NULL && (hT->cmp(key, next->key) == 0))
    {
        free(next->value);
        next->value = malloc(sizeof(void*));
        next->value = value;
    }
    else
    {
        e = CreateElement(key, value);
        if(e == NULL)
        {
            fprintf(stderr, "Nu s-a alocat memorie pentru element(add)");
        }
        else
        {
            e->hash = h;

            if(next == hT->table[h])
            {
                e->next = next;
                hT->table[h] = e;
            }
            else if(next == NULL)
            {
                last->next = e;
            }
            else
            {
                e->next = next;
                last->next = e;
            }

            hT->nrElements++;

            if(hT->nrElements > hT->tableSize * TABLE_GROW_DENSITY)
            {
                ResizeHashTable(hT);
            }
        }
    }
}
void DeleteHashTableItem(HashTable *hT, const void *key, const void *value)
{
    unsigned long h = hT->hashFunction(key);
    int i = h % hT->tableSize;

    Element **previous;
    Element *e;

    for(previous = &(hT->table[i]); *previous != NULL; previous = &((*previous)->next))
    {
        if(!hT->cmp((*previous)->key, key))
        {
            e = *previous;
            *previous = e->next;

            free(e->key);
            free(e->value);
            free(e);

            return ;
        }
    }
}
int SearchHashTableItem(HashTable *hT, const void *key, const void *value)
{
    Element *e;

    e = GetHashTableElement(hT, key);
    if(e != NULL && !hT->cmp(e->value,value))
    {
        return 1;
    }
    return 0;
}
HashTable* ReHashTable(HashTable *hT, int (*hashFunction)(const void*))
{
    HashTable *hNew = CreateHashTable(hT->tableSize, hashFunction, hT->print, hT->cmp);
    if(hNew == NULL)
        return NULL;
    Element *e;

    for(int i = 0; i < hT->tableSize; i++)
    {
        if(hT->table[i] != NULL)
            for(e = hT->table[i]; e != NULL; e = e->next)
            {
                void *key = (void*) e->key;
                void *value = (void*) e->value;
                AddHashTableItem(hNew, key, value);
            }
    }

    DeleteHashTable(hT);
    return hNew;
}
void DeleteHashTable(HashTable *hT)
{
    Element *e;
    Element *next;

    for(int i = 0; i < hT->tableSize; i++)
    {
        for(e = hT->table[i]; e != 0 ; e = next)
        {
            next = e->next;
            free(e);
        }
    }
    free(hT->table);
    free(hT);
}


#define RESIZE 2
void ResizeHashTable(HashTable *hT)
{
    Element **oldTable;
    int oldSize;
    Element *e;
    Element *next;
    int newPos;

    /*save old table*/
    oldTable = hT->table;
    oldSize = hT->tableSize;

    /*make new table*/
    hT->tableSize *= RESIZE;
    hT->table = malloc(sizeof(hT->table) * hT->tableSize);
    if(hT->table == NULL)
    {
        hT->table = oldTable;
        hT->tableSize = oldSize;
        return;
    }
    /*clear new table*/
    for(int i=0; i < hT->tableSize; i++)
        hT->table[i] = 0;
    /*move all elements of old table to new one*/
    for(int i=0; i< oldSize; i++)
    {
        for(e = oldTable[i]; e != NULL; e = next)
        {
            next = e->next;
            /*find the position in the new table*/
            newPos = e->hash % hT->tableSize;
            e->next = hT->table[newPos];
            hT->table[newPos] = e;
        }
    }
    free(oldTable);
}

struct Element* GetHashTableElement(HashTable *hT, const void *key)
{
    unsigned long h;
    int i;
    Element *e;

    h = hT->hashFunction(key);
    i = h % hT->tableSize;
    for(e = hT->table[i]; e != NULL; e = e->next)
    {
        if(e->hash == h && hT->cmp(key, e->key) == 0)
            return e;
    }
    return NULL;
}


#define BASE (256)

int hashFunctionString (const char *s)
{
    unsigned long h;
    size_t m;
    unsigned const char *us;
    /*cast s to unsigned const char ensures that elements of s will be treated as having values >=0 */

    m = strlen(s);
    us = (unsigned const char *) s;
    h = 0;
    while(*us != '\0')
    {
        h = (h * BASE + *us) % m;
        us++;
    }

    return h;
}

int hashFunctionInt (const void *x)
{
    return *((int*) x);
}

int hashFunctionInt2(const void *x)
{
    return *((int*) x) + 1;
}
