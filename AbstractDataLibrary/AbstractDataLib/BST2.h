#ifndef BST_H_INCLUDED
#define BST_H_INCLUDED

typedef struct nodeBST_s
{
    void* key;
    void *data;
    struct nodeBST_s *left;
    struct nodeBST_s *right;
}nodeBST;

typedef struct BST
{
    nodeBST *root;
    void (*print) (const void*, FILE*);
	int (*cmp) (const void*, const void*);
}BST;


BST* CreateBST(void (*print) (const void*, FILE*), int (*cmp) (const void*, const void*));

void PrintBST(BST* b, FILE *f);
void PrintBSTRecursive(nodeBST *root, FILE *f, void (*print) (const void*, FILE*),int level);

void PreorderBST(BST* b, FILE *f);
void PreorderBSTRecursive(nodeBST *root, FILE *f, void (*print) (const void*, FILE*));
void InorderBST(BST* b, FILE *f);
void InorderBSTRecursive(nodeBST *root, FILE *f, void (*print) (const void*, FILE*));
void PostorderBST(BST* b, FILE *f);
void PostorderBSTRecursive(nodeBST *root, FILE *f, void (*print) (const void*, FILE*));

void AddBSTItem(BST *b, void *key, void *data);
nodeBST* AddBSTItemRecursive(nodeBST *root, void *key, void *data, int (*cmp) (const void*, const void*));

nodeBST* SearchBSTItem(BST *b, void *key);
nodeBST* SearchBSTItemRecursive(nodeBST *root, void *key, int (*cmp) (const void*, const void*));
nodeBST* DeleteBSTItem(BST *b, void *key);
nodeBST* DeleteBSTItemRecursive(nodeBST *root, void *key, int (*cmp) (const void*, const void*));

int HeightBST(BST *b);
int HeightBSTRecursive(nodeBST *root);

void DeleteBST(BST *b);
void DeleteBSTRecursive(nodeBST *root);

void MergeBSTs(BST *b1, BST *b2);
void MergeBSTsRecursive(nodeBST *root1, nodeBST *root2, int (*cmp) (const void*, const void*));

nodeBST* CreateNodeBst(void *key, void *data);
void PrintGivenLevel(nodeBST *root, FILE *f, void (*print) (const void*, FILE*), int level);
nodeBST* FindMinBST(nodeBST *root);
nodeBST* DeleteBSTItemReturnRoot(nodeBST *root, void *key, int (*cmp) (const void*, const void*));
#endif // BST_H_INCLUDED






/*BinarySearchTree (implementat ca înlănțuire de noduri)
Operații
a. CreateBST - creare instanță BinarySearchTree
b. PrintBST – afișarea elementelor sub formă de arbore
c. PreorderBST – parcurgere și afișare în preordine
d. InorderBST – parcurgere și afișare în inordine
e. PostorderBST – parcurgere și afișare în postordine
f. AddBSTItem – adăugarea unui nou element la arbore
g. SearchBSTItem – returnează elementul care are valoarea căutată
h. DeleteBSTItem – ștergerea și returnarea elementului cu valoarea specificată
i. HightBST – returnează înălțimea subarborelui specificat
j. DeleteBST – ștergerea elementelor și eliberarea memoriei
*/
//void (*print) (const void*, FILE*), int (*cmp) (const void*, const void*)
