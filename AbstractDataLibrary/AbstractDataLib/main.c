#include "AbstractData.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void printInt(const void* i, FILE *f)
{
    fprintf(f, "%d ", *((int*)i));
}

void printChar(const void* i, FILE *f)
{
    fprintf(f, "%c ", *((char*)i));
}

void printFloat(const void* i, FILE *f)
{
    fprintf(f, "%g ", *((float*)i));
}

void printString(const void *i, FILE *f)
{
    fprintf(f, "%.10s ", (char*)i);
}

int cmpNumbers(const void* i1, const void* i2)
{
    int item1 = *((int*)i1);
    int item2 = *((int*)i2);
    return (item1) - (item2);
}

int cmpStrings(const void* s1, const void* s2)
{
    //return strcmp((char*)i1, (char*)i2);
     while(*((char*)s1) && (*((char*)s1) == *((char*)s2)))
    {
        s1++;
        s2++;
    }
    return *((const unsigned char*)s1) - *((const unsigned char*)s2);
}

int indexVerification(int index, int size)
{
    if(index < 0)
    {
        fprintf(stderr, "Index negativ!\n");
        return 0;
    }
    if(index > (size - 1))
    {
        fprintf(stderr, "Indexul specificat este mai mare decat dimensiunea structurii de date!\n");
        return 0;
    }
    return 1;
}
