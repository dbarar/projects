#include "AbstractData.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

node *CreateNode(void *data)
{
    node *p;
    p=(node*)malloc(sizeof(node));
    if(p != NULL)
        p->data = data;
    else
    {
        fprintf(stderr, "Nu s-a putut aloca nodul!\n");
        free(p);
        return 0;
    }
    return p;
}

LinkedList* CreateLinkedList(void (*print) (const void*, FILE*), int (*cmp) (const void*, const void*))
{
    LinkedList *l = (LinkedList*) malloc(sizeof(LinkedList));
    if(l == NULL)
    {
        printf("Nu s-a putut aloca memorie pentru lista\n");
        return NULL;
    }
    l->head = l->tail = NULL;
    l->sizel = 0;
    l->print = print;
    l->cmp = cmp;
    return l;
}

int IsEmptyLinkedList(LinkedList *l)
{
    return (l->sizel == 0);
}
void PrintLinkedList(LinkedList *l, FILE *f)
{
    if(IsEmptyLinkedList(l))
        fprintf(stderr, "Lista vida (printare lista)!\n");
    else
    {
        node *iterator = l->head;
        do
        {
            l->print(iterator->data,f);
            iterator=iterator->next;
        }
        while(iterator!=NULL);
        fprintf(f,"\n");
    }
}

int AddLinkedListItem(LinkedList *l, void *item)
{
    node *temp = CreateNode(item);

    if(l == NULL)
    {
        fprintf(stderr, "Lista inexistenta!\n");
        return 0;
    }
    if(temp == NULL)
    {
        fprintf(stderr, "Nu s-a putut crea nodul temporar!\n");
        return 0;
    }

    if(l->head == NULL) //lista vida
    {
        l->head = temp;
        l->head->next = NULL;
        l->tail = l->head;
        l->tail->next = NULL;
    }
    else
    {
        l->tail->next = temp;
        l->tail = temp;
        l->tail->next = NULL;
    }

    (l->sizel)++;
    return 1;
}

int PutLinkedListItem(LinkedList *l, void *item, int index)
{
    if(index != l->sizel && !indexVerification(index, l->sizel))
    {
        return 0;
    }

    node *temp = CreateNode(item);

    if(temp == NULL)
    {
        fprintf(stderr, "Nu s-a creat nodul temporar!\n");
        return 0;
    }

    if(index == 0) //caz special head
    {
        temp->next = l->head;
        l->head = temp;
    }
    else if(index == l->sizel) //caz special tail
    {
        return AddLinkedListItem(l, item);
    }
    else
    {
        node *iterator = l->head;

        for(int i=0; i < index-1; i++,iterator = iterator->next);

        temp->next = iterator->next;
        iterator->next = temp;
    }

    (l->sizel)++;
    return 1;
}

node* GetLinkedListItem(LinkedList *l, int index)
{
    if(IsEmptyLinkedList(l))
    {
        fprintf(stderr, "Lista vida, nu se poate lua niciun element!\n");
        return NULL;
    }

    if(!indexVerification(index, l->sizel))
        return NULL;

    if(index == 0) //caz special head
    {
        return l->head;
    }
    else if(index == l->sizel) //caz special tail
    {
        return l->tail;
    }
    else
    {
        node *iterator = l->head;

        for(int i=0;i<index;i++)
            iterator = iterator->next;

        return iterator;
    }
    return NULL;
}

node* DeleteLinkedListItem(LinkedList *l, void *item)
{
    if(IsEmptyLinkedList(l))
    {
        fprintf(stderr, "Lista vida, nu se poate sterge niciun element!\n");
        return NULL;
    }

    node *iterator;
    node *precedent;

    for(iterator = l->head, precedent = l->head; (iterator != NULL) && l->cmp(item, iterator->data); iterator = iterator->next)
    {
        precedent = iterator;
    }

    if( iterator == NULL)
    {
        fprintf(stderr, "Elementul nu a fost gasit, deci nu se poate sterge!\n");
        return NULL;
    }
    else
    {
        if(iterator == l->head) //caz special head
        {
            l->head = iterator->next;
        }
        if(iterator == l->tail) //caz special tail
        {
            l->tail = precedent;
        }

        precedent->next = iterator->next;
        (l->sizel)--;
        return iterator;
    }
}

node* SearchLinkedListItem(LinkedList *l, void *item)
{
    if(l->sizel == 0)
    {
        fprintf(stderr, "Lista vida, cautare esuata!\n");
        return NULL;
    }

    node *temp=CreateNode(item);

    if(temp == NULL)
    {
        fprintf(stderr, "Nu s-a putut crea elementul temporar!\n");
        free(temp);
        return 0;
    }

    for(node *iterator = l->head; iterator != NULL; iterator = iterator->next)
    {
        if(l->cmp(item, iterator->data) == 0)
            return iterator;
    }

    fprintf(stderr, "Nu s-a gasit elementul in lista!\n");
    return NULL;
}

void SortLinkedList(LinkedList *l)
{
    if(l->sizel == 0 || l == NULL)
    {
        fprintf(stderr, "Lista vida sau lista inexistenta, nu se poate sorta!\n");
    }
    else
    {
        node *iterator;
        node *previous; //precedentul lui iterator
        node *iterator2;//urmatorul lui iterator
        int ok=0;
        do
        {
            ok=0;
            for(iterator = l->head, previous = l->head,iterator2 = iterator->next; iterator2 != NULL; iterator2 = iterator2->next)
            {
                if(l->cmp(iterator->data, iterator2->data) > 0)
                {
                    ok=1;
                    previous->next = iterator2;
                    iterator->next = iterator2->next;
                    iterator2->next = iterator;
                }
                previous = iterator;
                iterator = iterator2;
            }
        }
        while(ok);
    }
    //set tail

    for(node *iterator = l->head; iterator != NULL; iterator = iterator->next)
    {
        l->tail = iterator;
    }

}

int DeleteLinkedList(LinkedList *l)
{
    if(l == NULL)
    {
        fprintf(stderr,"Lista inexistenta!\n");
        return 0;
    }
    if(l->sizel != 0)
    {
        node *iterator1 = l->head;
        while(iterator1 != NULL)
        {
            node *iterator2 = iterator1->next;
            free(iterator1);
            iterator1 = iterator2;
        }
    }
    l->head = NULL;
    l->tail = NULL;
    l->sizel = 0;
    free(l);
    return 1;
}

int MergeLinkedLists(LinkedList *l, LinkedList *ll)
{
    if(l == NULL)
    {
        fprintf(stderr, "Lista inexistenta!\n");
        return 0;
    }
    if(ll == NULL)
    {
        fprintf(stderr, "Lista inexistenta!\n");
        return 0;
    }

    if(l->head == NULL) //lista vida
    {
        l->head = ll->head;
        l->tail = ll->tail;
    }
    else
    {
        l->tail->next = ll->head;
        l->tail = ll->tail;
    }

    (l->sizel) += ll->sizel;
    return 1;
}
