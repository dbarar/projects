
import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JLabel;

public class ControlPanel {

	private static final Insets regularInsets = new Insets(10, 10, 0, 10);

	private FourInARowFrame frame;

	private FourInARowModel model;

	private JPanel panel;
	private JLabel player1;
	private JLabel player2;

	public ControlPanel(FourInARowFrame frame, FourInARowModel model) {
		this.frame = frame;
		this.model = model;
		createPartControl();
	}

	private void createPartControl() {
		StartGameActionListener listener = new StartGameActionListener(frame, model);

		panel = new JPanel();
		panel.setLayout(new GridBagLayout());

		int gridy = 0;

		JButton startGameButton = new JButton("Start Game");
		startGameButton.addActionListener(listener);

		player1 = new JLabel("Player Red");
		player2 = new JLabel("Player Blue");

		addComponent(panel, startGameButton, 0, gridy++, 1, 1, regularInsets, GridBagConstraints.LINE_START, GridBagConstraints.HORIZONTAL);
		addComponent(panel, player1, 0, gridy++, 1, 1, regularInsets, GridBagConstraints.LINE_START, GridBagConstraints.HORIZONTAL);
		addComponent(panel, player2, 0, gridy++, 1, 1, regularInsets, GridBagConstraints.LINE_START, GridBagConstraints.HORIZONTAL);
	}

	private void addComponent(Container container, Component component, int gridx, int gridy, int gridwidth,
			int gridheight, Insets insets, int anchor, int fill) {
		GridBagConstraints gbc = new GridBagConstraints(gridx, gridy, gridwidth, gridheight, 1.0D, 1.0D, anchor, fill,
				insets, 0, 0);
		container.add(component, gbc);
	}

	public JPanel getPanel() {
		return panel;
	}
	
	public JLabel getLabel1(){
		return player1;
	}
	
	public JLabel getLabel2(){
		return player2;
	}
}