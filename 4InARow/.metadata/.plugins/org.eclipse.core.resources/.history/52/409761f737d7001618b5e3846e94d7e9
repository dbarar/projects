
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

public class FourInARowModel extends JPanel{

	private static final int FRAME_THICKNESS = 16;
	private static final int GRID_WIDTH = 8;
	
	private static final long   serialVersionUID = 4019841629547494495L;
	private GameOverImage image;

	static Cell[][] grid = new Cell[8][8];
	
	static Cell.Player player;
	
	private boolean mouseActive = false;
	
	public FourInARowModel() {
		this.image = new GameOverImage(this);
        this.image.run();
		initializeGrid();
		initializePlayer();
	}

	public void initializeGrid() {
		int xx = FRAME_THICKNESS;
		for (int x = 0; x < GRID_WIDTH; x++) {
			int yy = FRAME_THICKNESS;
			for (int y = 0; y < GRID_WIDTH; y++) {
				Cell cell = new Cell(Cell.Player.NONE);
				cell.setCellLocation(xx, yy);
				grid[x][y] = cell;
				yy += FRAME_THICKNESS + Cell.getCellWidth();
			}
			xx += FRAME_THICKNESS + Cell.getCellWidth();
		}
	}
	
	public void initializePlayer(){
		player = Cell.Player.RED;
	}

	public Cell.Player alternatePlayer(Cell.Player player){
		return (player == Cell.Player.RED) ? Cell.Player.BLUE : Cell.Player.RED;
	}
	
	public void addCell(int y){
		if(!isGameOver()){
			if(!isGridFull()){
				for(int i = 0; i < GRID_WIDTH; i++){
					if(grid[i][y].isZeroValue()){
						grid[i][y].setValue(player);
						player = alternatePlayer(player);
					}
				}					
			}
		}
	}
	
	public Cell getCell(int x, int y) {
		return grid[x][y];
	}

	public boolean isGameOver() {
		return isGridFull();
	}

	private boolean isGridFull() {
		for (int x = 0; x < GRID_WIDTH; x++) {
			for (int y = 0; y < GRID_WIDTH; y++) {
				if (grid[x][y].isZeroValue()) {
					return false;
				}
			}
		}
		return true;
	}

	public void setMouseActive(boolean mouseActive){
		this.mouseActive = mouseActive;
	}
	
	public Dimension getPreferredSize() {
		int width = GRID_WIDTH * Cell.getCellWidth() + FRAME_THICKNESS * 9;
		return new Dimension(width, width);
	}

	public void draw(Graphics g) {
		g.setColor(Color.DARK_GRAY);
		Dimension d = getPreferredSize();
		g.fillRect(0, 0, d.width, d.height);

		for (int x = 0; x < GRID_WIDTH; x++) {
			for (int y = 0; y < GRID_WIDTH; y++) {
				{
					grid[x][y].draw(g);
				}
			}
		}
	}
	
	@Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.draw(g);
         
        if (this.isGameOver()) {
            g.drawImage(image.getImage(), 0, 0, null);
        }
    }
}
