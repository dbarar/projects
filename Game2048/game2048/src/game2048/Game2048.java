package game2048;

import javax.swing.SwingUtilities;

public class Game2048 implements Runnable {
	@Override
	 public void run() {
	        new Game2048Frame(new Game2048Model());
	    }
	    
	 public static void main(String[] args) {
	        SwingUtilities.invokeLater(new Game2048());
	    }
}
/*The Runnable interface should be implemented by any class whose instances are intended to be executed by a thread. 
 *The class must define a method of no arguments called run.
 **/
/*invokeLater: Causes doRun.run() to be executed asynchronously on the AWT event dispatching thread. 
 * This will happen after all pending AWT events have been processed. 
 * This method should be used when an application thread needs to update the GUI. 
 * In the following example the invokeLater call queues the Runnable object doHelloWorld on the event dispatching thread and then prints a message.
 **/
