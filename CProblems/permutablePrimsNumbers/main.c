#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define EPSILON 0.001
/*Prime permutabile. Scrieți un program C care cere utilizatorului introducerea a două numere întregi pozitive - valori
care determină un interval.
Programul trebuie să determine și să afişeze toate numerele prime permutabile circular
din intervalul dat. Afișarea se va face respectând șablonul dat mai jos. Folosiți notația specifică pointerilor și expresii
cu pointeri.
Observație: Un număr prim permutabil circular este un număr prim care îşi păstrează proprietatea de a rămâne prim
pentru toate permutările circulare ale cifrelor sale.
Se vor folosi doar variabile locale, funcţii şi un stil de programare adecvat.
Exemplu de rulare și șablon pentru afișare:
-----------------------------------------------------------------------------
Introduceţi numerele intregi care determină intervalul: 10 400
Numerele prime permutabile din intervalul [10, 400] sunt:
11, 13, 17, 31, 37, 71, 73, 79, 97,
113, 131, 199,
311, 337, 373
------------------------------------------------------------------------------
*/
void citire(int *a,int *b)
{
    do
    {
        printf("a= ");
        scanf("%d",a);
        printf("b= ");
        scanf("%d",b);
    }
    while(a<0||b<0||a==b);
    if(*a>*b)
    {
        int aux=*a;
        *a=*b;
        *b=aux;
    }
}

int prim(int nr)
{
    if(nr<=1)
        return 0;
    for(int i=2; i<sqrt(nr) + EPSILON; i++)
        if(nr%i==0)
            return 0;
    return 1;
}

int nr_cifre(int nr)
{
    int nr_c=0;
    while(nr)
    {
        nr/=10;
        nr_c++;
    }
    return nr_c;
}

int power(int nr)
{
    int p=1;
    while(nr>0)
    {
        p*=10;
        nr--;
    }
    return p;
}

void permuta(int *nr)
{
    int aux=0;
    int nr_c=nr_cifre(*nr);
    //aux=(int)pow(10,nr_c); //12      2   10  20
    aux=power(nr_c);
    aux/=10;
    aux*=((*nr)%10);
    (*nr)/=10;
    *nr+=aux;
}

int nr_prim_permutabil(int nr)
{
    int nr_c=nr_cifre(nr);
    while(nr_c>0)
    {
        if(!prim(nr))
            return 0;
        permuta(&nr);
        nr_c--;
    }
    return 1;
}

int ordin(int nr)
{
    int nr_c=nr_cifre(nr);
    return power(nr_c-1);
}

void afisare_nr_permutabile(int a,int b)
{
    int nr_c=0;
    int ord=ordin(b);
    printf("ord=%d \n",ord);
    for(int i=a,o=ord; i<=b; i++)
    {
        if(nr_prim_permutabil(i))
        {
            if(i>o-ord)
            {
                printf("\n");
                o+=ord;
            }
            printf("%d, ",i);
        }
        else
            if(i>o)
                o+=ord;
    }
}

int main()
{
    int a;
    int b;
    citire(&a,&b);
    afisare_nr_permutabile(a,b);
    return 0;
}
