#ifndef HEADER_H_INCLUDED
#define HEADER_H_INCLUDED

#define MAX_S 100
#define MAX_V 200

#endif // HEADER_H_INCLUDED
char* citire_sir();
void afisare_sir(char *s);
void eliminare_spatii_albe(char *s);
void eliminare_ultim_cuvant(char *s);
void eliminare_aparitie_subsir(char* s,char* subs);
void inserare_pozitie(char *s, int poz, char *subs);
void histograma_litere(char* s, int *v);
