#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*4.	Conversie bază și scriere în șir.
Scrieți o funcție itob(numar, sir, baza) care convertește un întreg (numar) în baza specificată (baza) și stochează reprezentarea numărului
 în șirul de caractere sir.*/
void f1(int nr, char sir[10], int b)
{
    int r;
    int nrs=0;
    int p=1;
    while(nr>0)
    {
        r=nr%b;
        nrs=nrs+p*r;
        p*=10;
        nr/=b;
    }
    printf("%d",nrs);
    sscanf(sir,"%s",nrs);
}

void f2(int nr, char sir[10], int b)
{
    itoa(nr,sir,b);
}

void f3(int nr, char sir[10], int b)
{
    int r;
    int nrs=0;
    int p=1;
    int i=0;
    while(nr>0)
    {
        r=nr%b;

        char x;
        if(r<=9)
            x=r+'0';
        else
            x=r+'a'-10;
        sir[i++]=x;

        nr/=b;
    }
    strrev(sir);
}
int main()
{
    int nr;
    int b;
    char sir[10]="";
    printf("numar: baza: ");
    scanf("%d",&nr);
    scanf("%d",&b);

    f1(nr,sir,b);
    printf("%s\n",sir);

    f2(nr,sir,b);
    printf("%s\n",sir);

    f3(nr,sir,b);
    printf("%s\n",sir);
    return 0;
}
