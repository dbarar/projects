#include <stdio.h>
#include <stdlib.h>
#define MAX_V 100
/*7. Copierea unui fisier text sursă într-un fișier destinație in ordinea inversă a liniilor.
(numele fișierelor sunt specificate din linia de comandă) */
int main(int argc, char **argvs)
{
    FILE *f,*g;
    f=fopen(argvs[1],"r");
    g=fopen(argvs[2],"w");
    if(f==NULL)
    {
        printf("Fisierul nu s-a putut deshide!");
        exit(1);
    }
    if(g==NULL)
    {
        printf("Fisierul nu s-a putut deshide!");
        exit(1);
    }
    int v[MAX_V];
    int n=0;
    char c;
    char s[100];
    while(!feof(f))
    {
        if((c=fgetc(f))=='\n')
            v[n++]=ftell(f);
    }
    for(int i=n-2;i>=0;i--)
    {
        fseek(f,v[i],SEEK_SET);
        fscanf(f,"%[^\n]s",s);
        fprintf(g,"%s\n",s);
    }
    fseek(f,0,SEEK_SET);//pentru prima linie
    fscanf(f,"%[^\n]s",s);
    fprintf(g,"%s",s);
    return 0;
}
