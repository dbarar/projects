#include <stdio.h>
#include <stdlib.h>
# include "h.h"
/*Dezvoltati modulul tablou_dinamic care contine urmatoarele functii (considerand ca elementele sunt de tip intreg):

alocare_matrice - citirea dimensiunilor si alocarea dinamica a spatiului pentru matrice
citire_elemente - popularea matricei cu elemente date linie cu linie
adaugare_linie - adaugarea unei noi linii la matrice dupa o linie specificata prin numar de ordine si citirea noilor elemente
adaugare_coloana - adaugarea unei noi coloane la matrice dupa o coloana specificata prin numar de ordine si citirea noilor elemente
adaugare_inel - expandarea dimensiunii matricei astfel incat vechea matrice sa fie inscrisa intr-un inel exterior si citirea elementelor
 de pe inel afisare matrice - afiseaza matricea linie cu linie


Scrieti o aplicatie C care foloseste modulul de mai sus si permite gestiunea tablourilor bidimensionale de intregi (meniu cu optiuni).

Last modified: Wednesday, 23 November 2016, 12:48 PM*/
int main()
{
    printf("n=");
    scanf("%d",&n);
    printf("m=");
    scanf("%d",&m);
    int **v=aloca_matrice(n,m);
    if(v==NULL)
        printf("nu s-a alocat matricea");
    citire_matrice(v,n,m);
    afisare_matrice(v,n,m);

    adauga_inel(&v,&n,&m);
    afisare_matrice(v,n,m);

    adauga_linie(&v,&n,m,3);
    afisare_matrice(v,n,m);

    adauga_coloana(&v,n,&m,3);
    afisare_matrice(v,n,m);


    return 0;
}
