#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "header.h"
#include<math.h>
carte* alocaCarte(int n)
{
    carte *c=malloc(n*sizeof(carte));
    return c;
}
void realocaCarte(carte **pagini,int n)
{
    carte *c=realloc(*pagini,n*sizeof(carte));
    *pagini=c;
}
void citireInregistrare(carte *p,FILE*f)
{
    char *s_tel=(char*)malloc(20);
    fscanf(f,"%s \n %s \n",p->nume,p->prenume);
    fscanf(f,"%[^\n]s ",p->adresa);
    fgetc(f);
    fscanf(f,"%[^\n]s ",p->stat);
    fgetc(f);
    fscanf(f,"%s \n %s \n",s_tel,p->email);
    int nr1,nr2,nr3;
    sscanf(s_tel,"%d - %d - %d",&nr1,&nr2,&nr3);
    p->tel=nr1*pow(10,9)+nr2*pow(10,6)+nr3;
}
void citireNrTel(carte *p)
{
    strcpy(p->nume,"");
    strcpy(p->prenume,"");
    strcpy(p->adresa,"");
    printf("dati nr de tel: ");
    scanf("%d",&(p->tel));
}

void afisareInregistrare(carte *p)
{
    printf("afisareInreg:\n nume: %s\n prenume: %s\n adresa: %s\n stat: %s\n nrTel: %d\n email: %s\n",p->nume,p->prenume,p->adresa,p->stat,p->tel,p->email);
}
void afisarePaginiAurii(carte *pagini,int n)
{
    printf("afisarePagini: \n");
    for(int i=0;i<n-1;i++)
    {
        printf("%d\n",i);
        afisareInregistrare((pagini+i));
    }
}
void adaugare(carte**pagini,int *n,FILE*f)
{
    carte p;
    citireInregistrare(&p,f);
    *(*pagini+(*n)-1)=p;
    (*n)++;
    realocaCarte(pagini,*n);
}
int gasesteIndice(carte *pagini,int n, carte p)
{
    for(int i=0;i<n-1;i++)
    {
        if((!strcmp(pagini[i].nume,p.nume)&&!strcmp(pagini[i].prenume,p.prenume))||pagini[i].tel==p.tel)
            return i;
    }
    return -1;
}
void stergere(carte **pagini,int *n,carte p)
{
    int i=0;
    int indice=gasesteIndice(*pagini,n,p);
    if(indice==-1)
        exit(1);
    afisareInregistrare((*pagini)+indice);
    free((carte*)(*pagini)+indice);

    for(i=indice;i<(*n)-1;i++)
        *(*(pagini)+i)=*(*(pagini)+i+1);
    (*n)--;
}
void gasesteNume(carte *pagini,int n,char *nume)
{
    for(int i=0;i<n-1;i++)
    {
        if(!strcmp(pagini[i].nume,nume))
        {
            afisareInregistrare(&pagini[i]);
        }
    }
}
