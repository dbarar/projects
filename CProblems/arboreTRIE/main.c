#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NRLITERE 26


typedef struct trie
{
    struct trie *child[NRLITERE];
    char c;
    int end;
}trie;

trie * createNode()
{
    trie *node = (trie*)malloc(sizeof(trie));
    if(node != NULL)
    {
        node->end = 0;
        for(int i = 0; i < NRLITERE; i++)
        {
            node->child[i] = NULL;
        }
    }

    return node;
}

void adaugare(trie *root, char *s)
{
    int indice;
    trie *nodParcurg = root;
    for(int i = 0; i < strlen(s); i++)
    {
        indice = (int)s[i] - (int)'a';
        if(nodParcurg->child[indice] == NULL)
        {
            nodParcurg->child[indice] = createNode();
        }
        nodParcurg = nodParcurg->child[indice];
    }
    nodParcurg->end = 1;
}

int cautare(trie *root, char *s)
{
    trie *nodParcurg = root;
    int indice;

    for(int i = 0; i < strlen(s); i++)
    {
        indice = (int)s[i] - (int)'a';
        if(nodParcurg->child[indice] != NULL)
        {
            nodParcurg = nodParcurg->child[indice];
        }
        else
            return 0;
    }
    if(nodParcurg->end == 1)
        return 1;
    return 0;
}

void cuvintePrefix(trie *root, char *s)
{
    trie *nodParcurg = root;
    int indice;

    char cuvant[50];
    strcpy(cuvant, s);

    for(int i = 0; i < strlen(s); i++)
    {
        indice = (int)s[i] - (int)'a';
        if(nodParcurg->child[indice] != NULL)
        {
            if(nodParcurg->end == 1)
            {
                printf("%s\n", s);
            }
            nodParcurg = nodParcurg->child[indice];
        }
    }
}

int main()
{
    trie *root = createNode();

    FILE *f = fopen("cuvinte.in", "r");

    char cuvant[50];
    while(!feof(f))
    {
        fscanf(f, "%s ", cuvant);
        adaugare(root, cuvant);
    }

    printf("dati cuvantul: ");
    scanf("%s", cuvant);
    printf("cuvantul: %s gasit: %d", cuvant, cautare(root, cuvant));
    return 0;
}
