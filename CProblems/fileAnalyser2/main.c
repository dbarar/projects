#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_CUV 20
#define MAX_CAR_LINIE 50
#define MAX_LINII 100
#define MAX_TERMENI 200
/*5. Analizor de fișier – varianta cu mai multe fișiere de intrare din linia de comandă.
Modificați codul aferent analizorului astfel încât să poată procesa mai multe fișiere de intrare,
furnizate din linia de comandă ca argumente ale programului și să permită calcularea următoarelor statistici:
 cel mai frecvent cuvânt din toate fișierele de intrare (dacă sunt mai multe cuvinte cu aceeași frecvență de apariție se afișează toate)
    împreună cu numărul de apariții al cuvântului în fiecare dintre fișierele de intrare
 histograma de apariție a cuvintelor considerând toate fișierele de intrare (sub forma cuvânt – nr. apariții)
 numele fișierelor și numărul liniilor în care apare un cuvânt căutat*/

typedef struct termen
{
    int v[MAX_LINII];
    int i_v;
    char cuvant[MAX_CUV];
}termen;
typedef struct fisier_type
{
    int n;//numarul de termeni din vectorul t
    termen *t;
    char nume_fisier[20];
}fisier_type;
int cauta_termen(termen t[], int n, char s[])
{
    for(int i=0;i<n;i++)
        if(strcmp(t[i].cuvant,s)==0)
            return i;
    return -1;
}
void afisare(termen *t, int n)
{
    for(int i=0;i<n;i++)
    {
        printf("\ncuvantul %s apare de %d ori pe liniile: ",t[i].cuvant,t[i].i_v);
        for(int j=0;j<t[i].i_v;j++)
            printf("%d ",t[i].v[j]);
    }
}
void stergere_ultimul_cuv(termen *t,int n)
{
    t[n-1].v[t[n-1].i_v-1]=0;
    t[n-1].i_v--;
}
void parcurge(FILE *f, termen *t, int *n)
{
    char *lstring=(char*)malloc(MAX_CAR_LINIE);
    char *s=(char*)malloc(MAX_CUV);
    int linie=0;
    while(!feof(f))
    {
        fgets(lstring,MAX_CAR_LINIE,f);            /*cu se evita citirea ultimului cuv*/
        //fscanf(f,"%50s ",lstring);              /***in loc de 50 cum se pune constanta MAX_CAR_LINIE?***/
        linie++;
        int cantitate=0;
        while(sscanf(lstring+cantitate,"%s",s)==1)
        {
            cantitate+=strlen(s)+1;
            int i=cauta_termen(t,*n,s);
            if(i!=-1)
            {
                (t+i)->v[(t+i)->i_v]=linie;
                (t+i)->i_v++;
            }
            else//creeam alt termen
            {
                strcpy(t[*n].cuvant,s);
                t[*n].i_v=0;
                t[*n].v[t[*n].i_v]=linie;
                (t[*n].i_v)++;
                (*n)++;
            }
        }
    }
    stergere_ultimul_cuv(t,*n);
}
void sortare_nr_aparitii(termen *t, int n)
{
    int ok=0;
    do
    {
        ok=0;
        for(int i=0;i<n-1;i++)
            if(t[i].i_v<t[i+1].i_v)
            {
                termen aux=t[i];
                t[i]=t[i+1];
                t[i+1]=aux;
                ok=1;
            }
        n--;
    }
    while(ok);
}
void cel_mai_frecvent(termen *t, int n, int nr_f)//dintr-un fisier
{
    int max_f=t[0].i_v;
    int i;
    for(i=1;i<n&&max_f==t[i].i_v;i++);
    for(int j=0;j<i;j++)
        printf("termenul cel mai frecvent din fisierul cu nr %d este %s si apare de %d ori\n",nr_f,t[j].cuvant,t[j].i_v);
}
void cel_mai_frecvent_din_fisiere(fisier_type *fv, int n_f)
{
    for(int i=0;i<n_f;i++)
        cel_mai_frecvent(fv[i].t,fv[i].n,i+1);
}
void histograma_cuvinte(fisier_type *fv, int n_f)
{
    int cantitate=0;
    for(int i=0;i<n_f;i++)
    {
        cantitate+=fv[i].n;
    }
    termen *t=(termen*) malloc(sizeof(termen)*cantitate);
    int n=0;
    for(int i=0;i<n_f;i++)
    {
        for(int j=0;j<fv[i].n;j++)
        {
            int k=cauta_termen(t,n,fv[i].t[j].cuvant);
            if(k!=-1)//e deja in t
            {
                t[k].i_v+=fv[i].t[j].i_v;
            }
            else//nu il gaseste => il adauga
            {
                t[n]=fv[i].t[j];
                t[n].i_v=fv[i].t[j].i_v;
                n++;
            }
        }
    }
    afisare(t,n);
}
void cuv_cautat(fisier_type *fv, int n_f)
{
    char cuvant[MAX_CUV];
    printf("\ndati cuvantul: ");
    scanf("%s",cuvant);

    for(int i=0;i<n_f;i++)//care fisier
    {
        int k=cauta_termen(fv[i].t,fv[i].n,cuvant);
        if(k!=-1)//l-a gasit
        {
            printf("\ncuvantul %s apare in fisierul cu numele %s de %d ori pe liniile ",cuvant,fv[i].nume_fisier,fv[i].t[k].i_v);
            for(int j=0;j<fv[i].t[k].i_v;j++)
                printf("%d ",fv[i].t[k].v[j]);
        }
    }
}
int main(int argc, char *argvs[])
{
    FILE* f;
    fisier_type *fv=(fisier_type*)malloc(sizeof(fisier_type)*(argc));
    for(int i=0;i<argc-1;i++)
    {
        strcpy(fv[i].nume_fisier,argvs[i+1]);
        f=fopen(fv[i].nume_fisier,"r");
        if(f==NULL)
        {
            printf ("Fisierul nu se poate deschide!");
            exit(1);
        }
        else
        {
            //printf ("Fisierul s-a deschis!");
            fv[i].n=0;
            fv[i].t=(termen*)malloc(sizeof(termen)*MAX_TERMENI);
            parcurge(f,fv[i].t,&(fv[i].n));
            sortare_nr_aparitii(fv[i].t,fv[i].n);
            afisare(fv[i].t,fv[i].n);
            printf("\n");
        }
        fclose(f);
    }
    cel_mai_frecvent_din_fisiere(fv,argc-1);
    histograma_cuvinte(fv,argc-1);
    cuv_cautat(fv,argc-1);
    return 0;
}

