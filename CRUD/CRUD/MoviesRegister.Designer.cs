﻿namespace CRUD
{
    partial class MoviesRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Title = new System.Windows.Forms.Label();
            this.TitleText = new System.Windows.Forms.TextBox();
            this.Director = new System.Windows.Forms.Label();
            this.DirectorText = new System.Windows.Forms.TextBox();
            this.Genre = new System.Windows.Forms.Label();
            this.GenreText = new System.Windows.Forms.TextBox();
            this.CharacterText = new System.Windows.Forms.TextBox();
            this.Character = new System.Windows.Forms.Label();
            this.CreateBtn = new System.Windows.Forms.Button();
            this.ReadBtn = new System.Windows.Forms.Button();
            this.UpdateBtn = new System.Windows.Forms.Button();
            this.DeleteBtn = new System.Windows.Forms.Button();
            this.IDText = new System.Windows.Forms.TextBox();
            this.ID = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.StatusText = new System.Windows.Forms.TextBox();
            this.Clear = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Location = new System.Drawing.Point(803, 69);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(27, 13);
            this.Title.TabIndex = 0;
            this.Title.Text = "Title";
            // 
            // TitleText
            // 
            this.TitleText.Location = new System.Drawing.Point(806, 86);
            this.TitleText.Name = "TitleText";
            this.TitleText.Size = new System.Drawing.Size(318, 20);
            this.TitleText.TabIndex = 1;
            // 
            // Director
            // 
            this.Director.AutoSize = true;
            this.Director.Location = new System.Drawing.Point(803, 129);
            this.Director.Name = "Director";
            this.Director.Size = new System.Drawing.Size(63, 13);
            this.Director.TabIndex = 0;
            this.Director.Text = "Film director";
            // 
            // DirectorText
            // 
            this.DirectorText.Location = new System.Drawing.Point(806, 146);
            this.DirectorText.Name = "DirectorText";
            this.DirectorText.Size = new System.Drawing.Size(318, 20);
            this.DirectorText.TabIndex = 2;
            // 
            // Genre
            // 
            this.Genre.AutoSize = true;
            this.Genre.Location = new System.Drawing.Point(803, 183);
            this.Genre.Name = "Genre";
            this.Genre.Size = new System.Drawing.Size(36, 13);
            this.Genre.TabIndex = 0;
            this.Genre.Text = "Genre";
            // 
            // GenreText
            // 
            this.GenreText.Location = new System.Drawing.Point(806, 200);
            this.GenreText.Name = "GenreText";
            this.GenreText.Size = new System.Drawing.Size(318, 20);
            this.GenreText.TabIndex = 3;
            // 
            // CharacterText
            // 
            this.CharacterText.Location = new System.Drawing.Point(806, 256);
            this.CharacterText.Name = "CharacterText";
            this.CharacterText.Size = new System.Drawing.Size(318, 20);
            this.CharacterText.TabIndex = 4;
            // 
            // Character
            // 
            this.Character.AutoSize = true;
            this.Character.Location = new System.Drawing.Point(803, 240);
            this.Character.Name = "Character";
            this.Character.Size = new System.Drawing.Size(78, 13);
            this.Character.TabIndex = 2;
            this.Character.Text = "Main character";
            // 
            // CreateBtn
            // 
            this.CreateBtn.Location = new System.Drawing.Point(176, 316);
            this.CreateBtn.Name = "CreateBtn";
            this.CreateBtn.Size = new System.Drawing.Size(112, 58);
            this.CreateBtn.TabIndex = 6;
            this.CreateBtn.Text = "Create";
            this.CreateBtn.UseVisualStyleBackColor = true;
            this.CreateBtn.Click += new System.EventHandler(this.CreateBtn_Click);
            // 
            // ReadBtn
            // 
            this.ReadBtn.Location = new System.Drawing.Point(320, 316);
            this.ReadBtn.Name = "ReadBtn";
            this.ReadBtn.Size = new System.Drawing.Size(112, 58);
            this.ReadBtn.TabIndex = 7;
            this.ReadBtn.Text = "Read";
            this.ReadBtn.UseVisualStyleBackColor = true;
            this.ReadBtn.Click += new System.EventHandler(this.ReadBtn_Click);
            // 
            // UpdateBtn
            // 
            this.UpdateBtn.Location = new System.Drawing.Point(472, 316);
            this.UpdateBtn.Name = "UpdateBtn";
            this.UpdateBtn.Size = new System.Drawing.Size(112, 58);
            this.UpdateBtn.TabIndex = 8;
            this.UpdateBtn.Text = "Update";
            this.UpdateBtn.UseVisualStyleBackColor = true;
            this.UpdateBtn.Click += new System.EventHandler(this.UpdateBtn_Click);
            // 
            // DeleteBtn
            // 
            this.DeleteBtn.Location = new System.Drawing.Point(623, 316);
            this.DeleteBtn.Name = "DeleteBtn";
            this.DeleteBtn.Size = new System.Drawing.Size(109, 58);
            this.DeleteBtn.TabIndex = 9;
            this.DeleteBtn.Text = "Delete";
            this.DeleteBtn.UseVisualStyleBackColor = true;
            this.DeleteBtn.Click += new System.EventHandler(this.DeleteBtn_Click);
            // 
            // IDText
            // 
            this.IDText.Location = new System.Drawing.Point(806, 35);
            this.IDText.Name = "IDText";
            this.IDText.Size = new System.Drawing.Size(318, 20);
            this.IDText.TabIndex = 5;
            // 
            // ID
            // 
            this.ID.AutoSize = true;
            this.ID.Location = new System.Drawing.Point(803, 12);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(18, 13);
            this.ID.TabIndex = 8;
            this.ID.Text = "ID";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(24, 15);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(763, 263);
            this.dataGridView1.TabIndex = 11;
            this.dataGridView1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseClick);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(806, 366);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(318, 20);
            this.dateTimePicker1.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(803, 293);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Status";
            // 
            // StatusText
            // 
            this.StatusText.Location = new System.Drawing.Point(806, 316);
            this.StatusText.Name = "StatusText";
            this.StatusText.Size = new System.Drawing.Size(318, 20);
            this.StatusText.TabIndex = 5;
            // 
            // Clear
            // 
            this.Clear.Location = new System.Drawing.Point(43, 316);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(112, 58);
            this.Clear.TabIndex = 12;
            this.Clear.Text = "Clear";
            this.Clear.UseVisualStyleBackColor = true;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // MoviesRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1136, 404);
            this.Controls.Add(this.Clear);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.StatusText);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.IDText);
            this.Controls.Add(this.ID);
            this.Controls.Add(this.DeleteBtn);
            this.Controls.Add(this.UpdateBtn);
            this.Controls.Add(this.ReadBtn);
            this.Controls.Add(this.CreateBtn);
            this.Controls.Add(this.CharacterText);
            this.Controls.Add(this.Character);
            this.Controls.Add(this.GenreText);
            this.Controls.Add(this.Genre);
            this.Controls.Add(this.DirectorText);
            this.Controls.Add(this.Director);
            this.Controls.Add(this.TitleText);
            this.Controls.Add(this.Title);
            this.Name = "MoviesRegister";
            this.Load += new System.EventHandler(this.MoviesRegister_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.TextBox TitleText;
        private System.Windows.Forms.Label Director;
        private System.Windows.Forms.TextBox DirectorText;
        private System.Windows.Forms.Label Genre;
        private System.Windows.Forms.TextBox GenreText;
        private System.Windows.Forms.TextBox CharacterText;
        private System.Windows.Forms.Label Character;
        private System.Windows.Forms.Button CreateBtn;
        private System.Windows.Forms.Button ReadBtn;
        private System.Windows.Forms.Button UpdateBtn;
        private System.Windows.Forms.Button DeleteBtn;
        private System.Windows.Forms.TextBox IDText;
        private System.Windows.Forms.Label ID;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox StatusText;
        private System.Windows.Forms.Button Clear;
    }
}

