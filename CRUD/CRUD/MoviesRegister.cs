﻿using System;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUD
{
    public partial class MoviesRegister : Form
    {        
        private DataTable dt = new DataTable();
        private MySqlCommand cmd;
        private MySqlDataAdapter adapter;
        static string conString = "Server=localhost;Database=Scheduler;Uid=root;AllowZeroDateTime=True;";
        private MySqlConnection con = new MySqlConnection(conString);


        public MoviesRegister()
        {
            InitializeComponent();

            //DataGridView properties
            dataGridView1.ColumnCount = 7;
            dataGridView1.Columns[0].Name = "ID";
            dataGridView1.Columns[1].Name = "Title";
            dataGridView1.Columns[2].Name = "Director";
            dataGridView1.Columns[3].Name = "Genre";
            dataGridView1.Columns[4].Name = "Character";
            dataGridView1.Columns[5].Name = "Data";
            dataGridView1.Columns[6].Name = "Status";

            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            //Selection mode
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.MultiSelect = false;
        }

        private void MoviesRegister_Load(object sender, EventArgs e)
        {            
            load();
        }

        private void populate(string id, string title, string director, string genre, string character, string data, string status)
        {
            dataGridView1.Rows.Add(id, title, director, genre, character, data, status);
        }
        public void load()
        {
            dataGridView1.Rows.Clear();

            //SQL stetment
            string sql = "Select * from movies";
            cmd = new MySqlCommand(sql, con);

            //open con, retrieve, fill data grid view
            try
            {
                con.Open();

                adapter = new MySqlDataAdapter(cmd);
                adapter.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    populate(row[0].ToString(), row[1].ToString(), row[2].ToString(), row[3].ToString(), row[4].ToString(), row[5].ToString(), row[6].ToString());
                }

                dt.Rows.Clear();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void populate(string v1, string v2, string v3, string v4, string v5, DateTime dateTime, string v6)
        {
            throw new NotImplementedException();
        }

        private void add(string Title, string Director, string Genre, string Character, string Data, string Status)
        {
            string sql = "Insert into movies(Title, Director, Genre, Characters, Data, Status) values (@TitleText, @DirectorText, @GenreText, @CharacterText, @DataText, @StatusText)";
            //string sql = "Insert into movies(Title, Director, Genre, Characters, Data, Status) values ( "'" + Title + "','" + Director + "','" + Genre + "','" + Character + "','" + Data + "','" + Status + "');";

            cmd = new MySqlCommand(sql, con);

            cmd.Parameters.AddWithValue("@TitleText", Title);
            cmd.Parameters.AddWithValue("@DirectorText", Director);
            cmd.Parameters.AddWithValue("@GenreText", Genre);
            cmd.Parameters.AddWithValue("@CharacterText", Character);
            cmd.Parameters.AddWithValue("@DataText", Data);
            cmd.Parameters.AddWithValue("@StatusText", Status);
            try
            {
                con.Open();
                if(cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Successfully inserted");
                }
            }
            catch(Exception ex)
            {
                clearTexts();
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.Close();
                load();
            }
        }

        private void update(int id, string Title, string Director, string Genre, string Character, string Data, string Status)
        {
            //sql stmt
            //string sqlCom=$"update movies set data='{Data}'"
            string sql = "update movies set Title='" + Title + "', Director='" + Director + "',Genre='" + Genre + "',Characters='" + Character + "',Data='" + Data + "',Status='" + Status + "' where MovieID = " + id + "";
            cmd = new MySqlCommand(sql, con);

            try
            {
                con.Open();
                adapter = new MySqlDataAdapter(cmd);
                adapter.UpdateCommand = con.CreateCommand();
                adapter.UpdateCommand.CommandText = sql;
                if (adapter.UpdateCommand.ExecuteNonQuery()>0)
                {
                    clearTexts();
                    MessageBox.Show("Successfully updated");
                }
                
                load();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void delete(int id)
        {
            string sql = "Delete from movies where MovieID = " + id + "";
            cmd = new MySqlCommand(sql, con);
            try
            {
                con.Open();
                adapter = new MySqlDataAdapter(cmd);
                adapter.DeleteCommand = con.CreateCommand();
                adapter.DeleteCommand.CommandText = sql;
                

                if(MessageBox.Show("Sure?","Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    if(cmd.ExecuteNonQuery()>0)
                    {
                        clearTexts();
                        MessageBox.Show("Successfully deleted");
                    }
                }

                load();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void clearTexts()
        {
            IDText.Text = "";
            TitleText.Text = "";
            DirectorText.Text = "";
            GenreText.Text = "";
            CharacterText.Text = "";
            dateTimePicker1.Text = "";
            StatusText.Text = "";
        }

        private void CreateBtn_Click(object sender, EventArgs e)
        {
            add(TitleText.Text, DirectorText.Text, GenreText.Text, CharacterText.Text, ConvertSystemDateTimeToMysqlDate(dateTimePicker1.Value), StatusText.Text);
        }
        private string ConvertSystemDateTimeToMysqlDate(DateTime date)
        {
            return date.ToString("yyyy-MM-dd");
        }
        private void ReadBtn_Click(object sender, EventArgs e)
        {
            load();
        }

        private void UpdateBtn_Click(object sender, EventArgs e)
        {
            string selected = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            int id = Convert.ToInt32(selected);
            update(id, TitleText.Text, DirectorText.Text, GenreText.Text, CharacterText.Text,ConvertSystemDateTimeToMysqlDate(dateTimePicker1.Value) , StatusText.Text);
        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            string selected = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            int id = Convert.ToInt32(selected);
            delete(id);
        }

        private DateTime ConvertMysqlDataToSystemDateTime(string date)
        {
            return DateTime.Parse(date);
        }
        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                IDText.Text = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                TitleText.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
                DirectorText.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
                GenreText.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
                CharacterText.Text = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
                StatusText.Text = dataGridView1.SelectedRows[0].Cells[6].Value.ToString();
                dateTimePicker1.Value = ConvertMysqlDataToSystemDateTime(dataGridView1.SelectedRows[0].Cells[5].Value.ToString());                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                //MessageBox.Show(DateTime.Now.ToString());
            }
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            clearTexts();
        }
    }
}
