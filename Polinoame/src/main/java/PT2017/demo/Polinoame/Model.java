package PT2017.demo.Polinoame;

import java.util.Collections;
import java.util.List;

public class Model {

	private Polinom polinom1;
	private Polinom polinom2;
	private Polinom rest = new Polinom();


	public void createPolinom1(String s) {
		setPolinom1(new Polinom(s));
	}

	public void createPolinom2(String s) {
		setPolinom2(new Polinom(s));
	}

	public Polinom addPolinoame(Polinom polinom1, Polinom polinom2) {
		Polinom result = new Polinom();
		List<Monom> monoame1 = polinom1.getMonoame();
		List<Monom> monoame2 = polinom2.getMonoame();
		for (Monom m : monoame1) {
			result.addMonom(m.getCoefficient(), m.getPower());
		}
		for (Monom m : monoame2) {
			result.addMonom(m.getCoefficient(), m.getPower());
		}

		Collections.sort(result.getMonoame());
		Collections.reverse(result.getMonoame());
		return result;
	}

	public Polinom subPolinoame(Polinom polinom1, Polinom polinom2) {
		Polinom result = new Polinom();
		List<Monom> monoame1 = polinom1.getMonoame();
		List<Monom> monoame2 = polinom2.getMonoame();
		for (Monom m : monoame1) {
			result.addMonom(m.getCoefficient(), m.getPower());
		}
		for (Monom m : monoame2) {
			result.addMonom(m.getCoefficient() * (-1), m.getPower());
		}

		Collections.sort(result.getMonoame());
		Collections.reverse(result.getMonoame());
		return result;
	}

	public Polinom multiplyPolinoame(Polinom polinom1, Polinom polinom2) {
		Polinom result = new Polinom();
		List<Monom> monoame1 = polinom1.getMonoame();
		List<Monom> monoame2 = polinom2.getMonoame();
		for (Monom m1 : monoame1) {
			for (Monom m2 : monoame2) {
				result.addMonom(m1.getCoefficient() * m2.getCoefficient(), m1.getPower() + m2.getPower());
			}
		}

		Collections.sort(result.getMonoame());
		Collections.reverse(result.getMonoame());
		return result;
	}

	
	public Polinom dividePolinoame(Polinom a, Polinom b) throws Exception{
		
		if(b.getFirstCoefficient() == 0)
			throw new ArithmeticException();
		
		Polinom result = new Polinom();
		
		while((a.degree() - b.degree()) >= 0 && !a.getMonoame().isEmpty()){
			Monom m = new Monom(a.getFirstCoefficient() / b.getFirstCoefficient(), a.degree() - b.degree());//impartim monomul cu gradul cel mai mare din deimpartit la monomul cu gradul cel mai mare din impartitor => un monom m, cu care o sa inmultim impartitorul 
			result.addMonom(m.getCoefficient(), m.getPower());//adaugam secvential in result monomul m
			
			Polinom temp = new Polinom();//construim un polinom care contine numai monomul m
			temp.addMonom(m.getCoefficient(), m.getPower());
			
			Polinom mResult = new Polinom();
			mResult = multiplyPolinoame(temp, b);//inmultim "monomul m" cu impartitorul
			
			int degBefore = a.degree();
			a = subPolinoame(a, mResult);//scadem din deimpartit mresult
			
			int degAfter = a.degree();
			if(degBefore == degAfter){//in cazul in care dupa efectuarea scaderii gradul deimpartitului ramane acelasi(a existat o impartire inexacta dintre doi intregi) vom reduce oricum gradul deimpartitului
				a.resetCoefficientLargestDegree();
			}
		}
		setRest(a);
		return result;
	}
	
	public Polinom dividePolinoameIntreg(Polinom a, Polinom b) throws Exception{
		if(b.getFirstCoefficient() == 0)
			throw new ArithmeticException();
		
		Polinom result = new Polinom();
		Polinom mResult = new Polinom();
		
		while((a.degree() - b.degree()) > 0 && !a.getMonoame().isEmpty() || ((a.degree() - b.degree()) == 0 && java.lang.Math.abs(a.getFirstCoefficient()) >= java.lang.Math.abs(b.getFirstCoefficient())) ){
			Monom m = new Monom(a.getFirstCoefficient() / b.getFirstCoefficient(), a.degree() - b.degree());//impartim monomul cu gradul cel mai mare din deimpartit la monomul cu gradul cel mai mare din impartitor => un monom m, cu care o sa inmultim impartitorul 
			result.addMonom((int)m.getCoefficient(), (int)m.getPower());//adaugam secvential in result monomul m
			
			Polinom temp = new Polinom();//contruim un polinom care contine numai monomul m
			temp.addMonom((int)m.getCoefficient(), (int)m.getPower());
							
			mResult = multiplyPolinoame(temp, b);//inmultim "monomul m" cu impartitorul
			
			a = subPolinoame(a, mResult);//scadem din deimpartit mresult
		}
		setRest(a);
		return result;
	}
	
	public Polinom derivatePolinom(Polinom polinom1){
		
		List<Monom> monoame = polinom1.getMonoame();
		for(Monom m : monoame){
			monoame.set(monoame.indexOf(m), new Monom(m.getCoefficient() * m.getPower(), m.getPower() - 1));
		}
		
		if(monoame.get(monoame.size() - 1).getPower() == -1){
			monoame.remove(monoame.size() - 1);
		}
		
		return polinom1;
	}
	
	public Polinom integratePolinom(Polinom polinom1){
		
		float coeff;
		List<Monom> monoame = polinom1.getMonoame();
		for(Monom m : monoame){
			coeff = m.getCoefficient() / (m.getPower() + 1);
			if(coeff != 0)
				monoame.set(monoame.indexOf(m), new Monom(coeff, m.getPower() + 1));
			else
				monoame.set(monoame.indexOf(m), new Monom(0, 0));
		}
		
		return polinom1;
	}

	public Polinom getPolinom1() {
		return polinom1;
	}

	public void setPolinom1(Polinom polinom1) {
		this.polinom1 = polinom1;
	}

	public Polinom getPolinom2() {
		return polinom2;
	}

	public void setPolinom2(Polinom polinom2) {
		this.polinom2 = polinom2;
	}

	public Polinom getRest() {
		return rest;
	}

	public void setRest(Polinom rest) {
		this.rest = rest;
	}
}